CXX=g++ -std=c++11

all: face2faceindex faceindex2directededge

face2faceindex: FaceToFaceIndex.cpp FaceToFaceIndex.h MeshTraversalUtility.h Float3.h
	$(CXX) -o face2faceindex FaceToFaceIndex.cpp

faceindex2directededge: FaceIndexToDirectedEdge.cpp FaceIndexToDirectedEdge.h MeshTraversalUtility.h Float3.h
	$(CXX) -o faceindex2directededge FaceIndexToDirectedEdge.cpp
