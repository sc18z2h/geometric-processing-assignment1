#include <iostream>
#include <string>
#include <sstream>
#include "FaceToFaceIndex.h"
using namespace std;

extern int faceindex_main(int argc, const char** argv);
extern int directededge_main(int argc, const char** argv);

int main(int argc, char** argv)
{
	const char* cmdArgs[] =
	{
		"",
		"-d",
		"./face indices/",
		"./soups/2ballout.tri",
		"./soups/2torus.tri",
		"./soups/5bout.tri",
		"./soups/747.tri",
		"./soups/badcube.tri",
		"./soups/badcycle.tri",
		"./soups/biplane.tri",
		"./soups/bitorus.tri",
		"./soups/cherrytree.tri",
		"./soups/cow.tri",
		"./soups/cow2.tri",
		"./soups/cube.tri",
		"./soups/dodecahedron.tri",
		"./soups/fuelinject.tri",
		"./soups/hexahedron.tri",
		"./soups/horse.tri",
		"./soups/icosahedron.tri",
		"./soups/legoman.tri",
		"./soups/many.tri",
		"./soups/octahedron.tri",
		"./soups/quadtorus.tri",
		"./soups/skeleton.tri",
		"./soups/tetrahedron.tri",
		"./soups/tetrahedron_bad.tri",
		"./soups/torus.tri",
		"./soups/tritorus.tri",
		"./soups/yoda.tri",
		"./soups/tetrahedron_single_line.tri",
		"./soups/pinch_vertex.tri",
		"./soups/pinch_edge.tri",
		"./soups/single_triangle.tri",
	};

	const char* cmdArgs2[] =
	{
		"",
		"-d",
		"./directed edges/",
		"./face indices/2ballout.face",
		"./face indices/2torus.face",
		"./face indices/5bout.face",
		"./face indices/747.face",
		"./face indices/badcube.face",
		"./face indices/badcycle.face",
		"./face indices/biplane.face",
		"./face indices/bitorus.face",
		"./face indices/cherrytree.face",
		"./face indices/cow.face",
		"./face indices/cow2.face",
		"./face indices/cube.face",
		"./face indices/dodecahedron.face",
		"./face indices/fuelinject.face",
		"./face indices/hexahedron.face",
		"./face indices/horse.face",
		"./face indices/icosahedron.face",
		"./face indices/legoman.face",
		"./face indices/many.face",
		"./face indices/octahedron.face",
		"./face indices/quadtorus.face",
		"./face indices/skeleton.face",
		"./face indices/tetrahedron.face",
		"./face indices/tetrahedron_bad.face",
		"./face indices/torus.face",
		"./face indices/tritorus.face",
		"./face indices/yoda.face",
		"./face indices/tetrahedron_single_line.face",
		"./face indices/pinch_vertex.face",
		"./face indices/pinch_edge.face",
		"./face indices/single_triangle.face",
	};

	faceindex_main(sizeof(cmdArgs) / sizeof(char*), cmdArgs);

	directededge_main(sizeof(cmdArgs2) / sizeof(char*), cmdArgs2);

	return EXIT_SUCCESS;
}

// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
