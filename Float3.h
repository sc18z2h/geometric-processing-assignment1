/*
///////////////////////////////////////////////////

	Zecheng Hu
	October, 2021

	------------------------
	Float3.h
	------------------------

	A common structure for storing and manipulating a vector of 3 floats.

///////////////////////////////////////////////////
*/

#pragma once

struct Float3
{
	float x, y, z;

	Float3() : x(0), y(0), z(0) { }

	Float3(float newX, float newY, float newZ) :
		x(newX), y(newY), z(newZ) { }
};

inline bool operator==(const Float3& lhs, const Float3& rhs)
{
	return lhs.x == rhs.x && lhs.y == rhs.y && lhs.z == rhs.z;
}

inline bool operator!=(const Float3& lhs, const Float3& rhs)
{
	return !(lhs == rhs);
}

inline Float3 operator+(const Float3& lhs, const Float3& rhs)
{
	return { lhs.x + rhs.x, lhs.y + rhs.y, lhs.z + rhs.z };
}

inline Float3 operator-(const Float3& lhs, const Float3& rhs)
{
	return { lhs.x - rhs.x, lhs.y - rhs.y, lhs.z - rhs.z };
}

inline void operator+=(Float3& lhs, const Float3& rhs)
{
	lhs.x += rhs.x;
	lhs.y += rhs.y;
	lhs.z += rhs.z;
}

inline void operator-=(Float3& lhs, const Float3& rhs)
{
	lhs.x -= rhs.x;
	lhs.y -= rhs.y;
	lhs.z -= rhs.z;
}

// useful for maps and sets
inline bool operator<(const Float3& lhs, const Float3& rhs)
{
	if (lhs.x != rhs.x)
		return lhs.x < rhs.x;
	else if (lhs.y != rhs.y)
		return lhs.y < rhs.y;
	else
		return lhs.z < rhs.z;
}

// useful for maps and sets
inline bool operator>(const Float3& lhs, const Float3& rhs)
{
	if (lhs.x != rhs.x)
		return lhs.x > rhs.x;
	else if (lhs.y != rhs.y)
		return lhs.y > rhs.y;
	else
		return lhs.z > rhs.z;
}