/*
///////////////////////////////////////////////////

	Zecheng Hu
	October, 2021

	------------------------
	FaceIndexToDirectedEdge.cpp
	------------------------

	Converts face-index format files (.face) into directed-edge files (.diredge).

///////////////////////////////////////////////////
*/

#include "FaceIndexToDirectedEdge.h"
#include "MeshTraversalUtility.h"
#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <set>
#include <stack>
#include <cstring>
using namespace std;

int main(int argc, const char** argv)
{
	if (argc < 2)
	{
		cout << "Usage: faceindex2directededge [-d <output directory>] file..." << endl;
		return EXIT_FAILURE;
	}
	
	int i = 1;
	bool hasOutputDirectory = false;
	if (strcmp(argv[1], "-d") == 0)
	{
		hasOutputDirectory = true;
		i = 3;
	}

	int exitCode = EXIT_SUCCESS;
	bool moreThan1File = i + 1 < argc;

	for (; i < argc; ++i)
	{
		string inFilePath(argv[i]);
		stringstream outFilePath;

		size_t lastDotIndex = inFilePath.find_last_of(".");

		if (hasOutputDirectory)
		{
			size_t lastForwardSlash = inFilePath.find_last_of("/");
			size_t lastBackSlash = inFilePath.find_last_of("\\");
			size_t lastSlashIndex = max(lastForwardSlash + 1, lastBackSlash + 1);
			outFilePath << argv[2] <<
				// file path without directory without extension
				inFilePath.substr(lastSlashIndex, lastDotIndex - lastSlashIndex) << 
				".diredge";
		}
		else
		{
			outFilePath <<
				// file path without extension
				inFilePath.substr(0, lastDotIndex) <<
				".diredge";
		}

		const char* lineSeperator = "====================================";
		if (moreThan1File)
			cout << "Converting (" << inFilePath << ")." << endl;

		if (ConvertFaceIndexToDirectedEdgeFile(inFilePath, outFilePath.str()))
			cout << "Conversion done." << endl;
		else
			exitCode = EXIT_FAILURE;

		if (moreThan1File)
			cout << lineSeperator << endl;
	}

	return exitCode;
}

bool ConvertFaceIndexToDirectedEdge(
	const std::vector<Float3>& inVertices, 
	const std::vector<unsigned int>& inFaces, 
	std::vector<unsigned int>& outFirstDirectedEdge, 
	std::vector<unsigned int>& outPairedDirectedEdge)
{
	outFirstDirectedEdge.clear();
	outPairedDirectedEdge.clear();
		
	outFirstDirectedEdge.resize(inVertices.size());
	outPairedDirectedEdge.resize(inFaces.size());

	// allDirectedEdgesFromVertex[vertexID] gives a vector of all edgeIDs "from" that vertex
	vector<vector<unsigned int>> allDirectedEdgesFromVertex;
	allDirectedEdgesFromVertex.resize(inVertices.size());

	// loop through all edges and populate allDirectedEdgesFromVertex
	// by adding each edge to their "from" vertex
	for (size_t edgeID = 0; edgeID < inFaces.size(); ++edgeID)
	{
		unsigned int vertexID = inFaces[edgeID];
		allDirectedEdgesFromVertex[vertexID].push_back(edgeID);

		// initialise pairs to -1 := unpaired
		outPairedDirectedEdge[edgeID] = static_cast<unsigned int>(-1);
	}

	// makes the FDE warning message appear only once
	bool allVerticesHaveFirstDirectedEdge = true;
	
	// populate outFirstDirectedEdge
	// using a vertex's first "from" edges
	for (size_t vertexID = 0; vertexID < outFirstDirectedEdge.size(); ++vertexID)
	{
		// make sure there are edges from the vertex
		if (0 < allDirectedEdgesFromVertex[vertexID].size())
			outFirstDirectedEdge[vertexID] = allDirectedEdgesFromVertex[vertexID][0];
		// otherwise there are unused vertices
		else if (allVerticesHaveFirstDirectedEdge)
		{
			allVerticesHaveFirstDirectedEdge = false;
			cout << "[warning] Vertex " << vertexID << " has no FDE" << endl;
		}
	}

	// makes the Directed Edge warning messages appear only once
	bool allEdgesShare2Faces = true;

	for (size_t i = 0; i < outPairedDirectedEdge.size(); ++i)
	{
		// if already paired
		if (outPairedDirectedEdge[i] != static_cast<unsigned int>(-1))
			continue;

		// we looking at this:
		// (vertexFrom) ==edge1From=> (vertexTo) ==edge1To=>
		unsigned int edge1From = i;
		unsigned int edge1To = TraverseDirectedEdgeAroundTriangle(edge1From);

		unsigned int vertexFromID = inFaces[edge1From];
		unsigned int vertexToID = inFaces[edge1To];
		vector<unsigned int>& edgesFromVertex = allDirectedEdgesFromVertex[vertexToID];
		
		// we need to find the directed edge going in the opposite direction
		// (vertexFrom) <=edge1From== (vertexTo)
		// that edge should be in edges "from" (vertexTo)
		for (size_t j = 0; j < edgesFromVertex.size(); ++j)
		{
			// we looking at this:
			// <=edge2To== (?) <=edge2From== (?)
			unsigned int edge2From = edgesFromVertex[j];
			unsigned int edge2To = TraverseDirectedEdgeAroundTriangle(edge2From);

			// if:
			// <=edge2To== (vertexFrom) <=edge2From== (vertexTo)
			if (vertexFromID == inFaces[edge2To] &&
				vertexToID == inFaces[edge1To])
			{
				// if the edge going in the opposite direction was already paired
				if (outPairedDirectedEdge[edge2From] != static_cast<unsigned int>(-1))
				{
					if (allEdgesShare2Faces)
					{
						allEdgesShare2Faces = false;
						int alreadyPairedEdge = static_cast<int>(outPairedDirectedEdge[edge2From]);
						cout << 
							"[warning] More than 2 faces per edge. Directed edges: " << 
							edge2From << ", " << 
							alreadyPairedEdge << ", " << 
							i << 
							" all describe the same edge." << endl;
					}
				}
				else
				{
					outPairedDirectedEdge[i] = edge2From;
					outPairedDirectedEdge[edge2From] = i;
				}
				break;
			}
		}

		// if no paired edge found
		if (allEdgesShare2Faces && outPairedDirectedEdge[i] == static_cast<unsigned int>(-1))
		{
			allEdgesShare2Faces = false;
			cout << "[warning] Less than 2 faces per edge. Directed edge: " << i << " is not paired." << endl;
		}
	}

	return allEdgesShare2Faces;
}

bool ReadFaceIndexFile(
	const std::string& inFilePath, 
	std::vector<Float3>& outVertices, 
	std::vector<unsigned int>& outFaces)
{
	outVertices.clear();
	outFaces.clear();
	
	ifstream fileStream(inFilePath);
	
	if (fileStream.is_open() == false)
	{
		cerr << "[Error] Couldn't open file '" << inFilePath << "'." << endl;
		return false;
	}

	string line;
	while (getline(fileStream, line))
	{
		if (line.size() == 0)
			continue;

		istringstream stringStream(line);
		string prefix;
		if (stringStream >> prefix &&
			0 < prefix.size() &&
			prefix[0] == '#') // check for comments
			continue;

		if (prefix == "Vertex")
		{
			unsigned int index = 0;
			Float3 newVertex;
			if (stringStream >> index >> newVertex.x >> newVertex.y >> newVertex.z)
			{
				// resize vertices if too small
				if (outVertices.size() <= index)
					outVertices.resize(index + 1);
				outVertices[index] = newVertex;
			}
		}
		else if (prefix == "Face")
		{
			unsigned int faceIndex = 0;
			unsigned int vertex1ID = 0, vertex2ID = 0, vertex3ID = 0;
			if (stringStream >> faceIndex >> vertex1ID >> vertex2ID >> vertex3ID)
			{
				unsigned startIndex = faceIndex * 3;
				// resize faces if too small
				if (outFaces.size() <= startIndex + 2)
					outFaces.resize(startIndex + 2 + 1);
				outFaces[startIndex] = vertex1ID;
				outFaces[startIndex + 1] = vertex2ID;
				outFaces[startIndex + 2] = vertex3ID;
			}
		}
	}

	// if we've reached the end of the file,
	// any formatting errors will stop reading before the end
	if (fileStream.eof())
		return true;
	else
	{
		cerr << "[Error] File has bad format '" << inFilePath << "'." << endl;
		return false;
	}
}

bool WriteDirectedEdgeFile(
	const std::vector<Float3>& inVertices, 
	const std::vector<unsigned int>& inFaces, 
	const std::vector<unsigned int>& inFirstDirectedEdge, 
	const std::vector<unsigned int>& inPairedDirectedEdge,
	const std::string& outFilePath)
{
	ofstream fileStream(outFilePath);

	if (fileStream.good())
	{
		// use the file name without directory and without extension as the object name
		size_t lastForwardSlash = outFilePath.find_last_of("/");
		size_t lastBackSlash = outFilePath.find_last_of("\\");

		size_t lastSlashIndex = max(lastForwardSlash + 1, lastBackSlash + 1);
		size_t lastDotIndex = outFilePath.find_last_of(".");
		string objectName = outFilePath.substr(lastSlashIndex, lastDotIndex - lastSlashIndex);

		fileStream <<
			"# University of Leeds 2021-2022" << endl <<
			"# COMP 5812M Assignment 1" << endl <<
			"# Zecheng Hu" << endl <<
			"# 201249146" << endl <<
			"#" << endl <<
			"# Object Name: " << objectName << endl <<
			"# Vertices=" << inVertices.size() << " Faces=" << inFaces.size() / 3 << endl <<
			"#" << endl;

		// float write format settings
		fileStream << fixed << setprecision(6);
		const char* SPACE = "  ";

		// make every vertex index the same width as the last index
		string lastVertexIndex = to_string(inVertices.size() - 1);
		size_t vertexIndexWidth = lastVertexIndex.size();
		for (size_t i = 0; i < inVertices.size(); ++i)
		{
			fileStream <<
				"Vertex " << setw(vertexIndexWidth) << i << SPACE <<
				inVertices[i].x << SPACE <<
				inVertices[i].y << SPACE <<
				inVertices[i].z << endl;
		}
		
		// make every FDE index the same width as the last index
		string lastFirstDirectedEdgeIndex = to_string(inFirstDirectedEdge.size() - 1);
		size_t firstDirectedEdgeIndexWidth = lastFirstDirectedEdgeIndex.size();
		// make every edge index the same width as the last index
		string lastEdgeIndex = to_string(inFaces.size() - 1);
		size_t edgeIndexWidth = lastEdgeIndex.size();
		for (size_t i = 0; i < inFirstDirectedEdge.size(); ++i)
		{			
			fileStream <<
				"FirstDirectedEdge " << setw(firstDirectedEdgeIndexWidth) << i << SPACE <<
				setw(edgeIndexWidth) << inFirstDirectedEdge[i] << endl;
		}

		// make every face index the same width as the last index
		string lastFaceIndex = to_string(inFaces.size() / 3 - 1);
		size_t faceIndexWidth = lastFaceIndex.size();
		for (size_t i = 0; i + 2 < inFaces.size(); i += 3)
		{
			fileStream <<
				"Face " << setw(faceIndexWidth) << i / 3 << SPACE <<
				setw(vertexIndexWidth) << inFaces[i] << SPACE <<
				setw(vertexIndexWidth) << inFaces[i + 1] << SPACE <<
				setw(vertexIndexWidth) << inFaces[i + 2] << endl;
		}

		for (size_t i = 0; i < inPairedDirectedEdge.size(); ++i)
		{
			fileStream <<
				"OtherHalf " << setw(faceIndexWidth) << i << SPACE <<
				setw(edgeIndexWidth) << inPairedDirectedEdge[i] << endl;
		}

		// file stream is only good when all writing has succeeded
		if (fileStream.good())
			return true;
	}
	cerr << "[Error] Couldn't write file '" << outFilePath << "'." << endl;
	return false;
}

bool ConvertFaceIndexToDirectedEdgeFile(
	const std::string& faceIndexFilePath, 
	const std::string& directedEdgeFilePath)
{
	bool isWritten = false;	
	vector<Float3> vertices;
	vector<unsigned int> faces;
	if (ReadFaceIndexFile(faceIndexFilePath, vertices, faces))
	{
		bool isManifold = false;
		vector<unsigned int> firstDirectedEdge;
		vector<unsigned int> pairedDirectedEdge;
		// this checks allEdgesShare2Faces
		if (ConvertFaceIndexToDirectedEdge(vertices, faces, firstDirectedEdge, pairedDirectedEdge))
		{
			if (CheckPinchVertices(vertices, faces, pairedDirectedEdge))
			{
				// manifold when : 1. allEdgesShare2Faces 2. no pinch vertices
				isManifold = true;
				// only calculate genus when mesh is connected, otherwise genus is a little meaningless
				if (IsConnected(vertices, faces, pairedDirectedEdge))
					cout << "Genus is " << GetGenus(vertices, faces) << "." << endl;
				else
					cout << "Not going to calculate genus because mesh isn't connected." << endl;
			}

			// this is a sanity check to make sure data structure is organised as expected
			// if this test fails, its probably a bug in the code
			if (CompareFaceIndexAndDirectedEdge(vertices, faces, firstDirectedEdge, pairedDirectedEdge) == false)
				cerr << 
					"[ERROR!] Sanity check failed! (" << faceIndexFilePath <<
					"). Face-index format isn't equivalent to directed-edge format. Probably a bug in the code." << endl;
		}

		isWritten = WriteDirectedEdgeFile(vertices, faces, firstDirectedEdge, pairedDirectedEdge, directedEdgeFilePath);

		cout << faceIndexFilePath;
		if (isManifold)
			cout << " IS manifold." << endl;
		else
			cout << " is NOT manifold." << endl;
	}
	return isWritten;
}

bool CheckPinchVertices(
	const std::vector<Float3>& vertices,
	const std::vector<unsigned int>& faces,
	const std::vector<unsigned int>& pairedDirectedEdge)
{
	// record the number of edge cycles around each vertex.
	vector<unsigned int> numberOfCyclesAroundVertex(vertices.size());
	// we only need to traverse through some edges once.
	vector<bool> isEdgeTraversed(faces.size());

	// loop through every directed edge
	for (size_t edgeID = 0; edgeID < faces.size(); ++edgeID)
	{
		if (isEdgeTraversed[edgeID])
			continue;

		// traverse directed edge around its "from" vertex.
		unsigned int aroundVertexID = faces[edgeID];
		unsigned int currentEdgeID = edgeID;		
		do
		{
			// if true, then this edge will produce the same cycle, we can eliminate it from the for loop.
			if(faces[currentEdgeID] == aroundVertexID)
				isEdgeTraversed[currentEdgeID] = true;

			currentEdgeID = TraverseDirectedEdgeAroundVertex(
				currentEdgeID, 
				aroundVertexID, 
				faces,
				pairedDirectedEdge);

			if (currentEdgeID == static_cast<unsigned int>(-1))
			{
				// something's gone horribly wrong
				cerr << 
					"[Error] CheckPinchPoints cannot traverse edge "<< currentEdgeID << 
					". Probably because not all edges has exactly 2 faces." << endl;
				return false;
			}
		} while (currentEdgeID != edgeID);

		++numberOfCyclesAroundVertex[aroundVertexID];
		// there should only be 1 cycle around each vertex,
		// we are guaranteed to have at least 1 cycle around this vertex due to the while loop
		if (1 < numberOfCyclesAroundVertex[aroundVertexID])
		{
			cout << "[warning] Pinch vertex found: " << aroundVertexID << "." << endl;
			return false;
		}
	}

	return true;
}

bool CompareFaceIndexAndDirectedEdge(
	const std::vector<Float3>& vertices, 
	const std::vector<unsigned int>& faces, 
	const std::vector<unsigned int>& firstDirectedEdge, 
	const std::vector<unsigned int>& pairedDirectedEdge)
{
	if (vertices.size() != firstDirectedEdge.size() ||
		faces.size() != pairedDirectedEdge.size())
		return false;

	// check FDEs are valid
	for (size_t i = 0; i < firstDirectedEdge.size(); ++i)
	{
		unsigned int edgeId = firstDirectedEdge[i];
		unsigned int vertexId = faces[edgeId];
		if (i != vertexId)
			return false;
	}

	// check paired directed edge go in opposite directions
	for (size_t i = 0; i < pairedDirectedEdge.size(); ++i)
	{
		size_t edge1From = i;
		unsigned int edge2From = pairedDirectedEdge[i];
		if (edge2From != static_cast<unsigned int>(-1))
		{
			//			   (vertex1From) ==edge1From=> (vertex1To) ==edge1To=>
			// <=edge2To==  (vertex2To)  <=edge2From== (vertex2From)
			unsigned int edge1To = TraverseDirectedEdgeAroundTriangle(edge1From);
			unsigned int edge2To = TraverseDirectedEdgeAroundTriangle(edge2From);

			unsigned int vertex1From = faces[edge1From];
			unsigned int vertex1To = faces[edge1To];

			unsigned int vertex2From = faces[edge2From];
			unsigned int vertex2To = faces[edge2To];

			// if they don't match in opposite directions
			if (faces[edge1From] != faces[edge2To] ||
				faces[edge1To] != faces[edge2From] ||
				edge1From != pairedDirectedEdge[edge2From]) // check pairs are doubly linked
				return false;
		}
	}

	return true;
}

bool IsConnected(
	const std::vector<Float3>& vertices, 
	const std::vector<unsigned int>& faces,
	const std::vector<unsigned int>& pairedDirectedEdge)
{
	// if there is less than 1 triangle face, we can't explore face 0
	if (vertices.size() < 3)
		return false;

	set<unsigned int> exploredFaces;
	// faces that need exploration
	stack<unsigned int> unexploredFaces;

	unexploredFaces.push(0);
	do
	{
		unsigned int faceID = unexploredFaces.top();
		unexploredFaces.pop();

		if (exploredFaces.find(faceID) != exploredFaces.end())
			continue;
		exploredFaces.insert(faceID);

		// find the 3 vertices in face/triangle
		unsigned int edgeID1 = faceID * 3;
		unsigned int edgeID2 = faceID * 3 + 1;
		unsigned int edgeID3 = faceID * 3 + 2;
		
		// explore connected neighbouring faces
		// vertexID/3 = faceID
		unexploredFaces.push(pairedDirectedEdge[edgeID1] / 3);
		unexploredFaces.push(pairedDirectedEdge[edgeID2] / 3);
		unexploredFaces.push(pairedDirectedEdge[edgeID3] / 3);
	} while (0 < unexploredFaces.size());

	// if connected, then we should have explored every vertex
	return exploredFaces.size() == faces.size() / 3;
}

int GetGenus(
	const std::vector<Float3>& vertices,
	const std::vector<unsigned int>& faces)
{	
	int v = vertices.size();
	// in faces vector each entry is 1 directed edge, 1 normal edge = 2 directed edges
	int e = faces.size() / 2;
	// in faces vector each triplet represents 1 face
	int f = faces.size() / 3;

	// using Euler's formula
	return 1 + (-v + e - f) / 2;
}