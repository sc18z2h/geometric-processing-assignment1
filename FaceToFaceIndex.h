/*
///////////////////////////////////////////////////

	Zecheng Hu
	October, 2021

	------------------------
	FaceToFaceIndex.h
	------------------------

	Converts face format files (polygon soup) into face-index files (.face).

///////////////////////////////////////////////////
*/

#pragma once
#include <vector>
#include <string>
#include "Float3.h"

/// <summary>
/// Converts face format (polygon soup) to face-index format.
/// O(n log(n)) where n is the number of vertices.
/// </summary>
/// <param name="inRawVertices">vector of non-unique vertex positions, each triplet is a triangle (polygon soup)</param>
/// <param name="outVertices">vector of unique vertex positions</param>
/// <param name="outFaces">vector of vertex index triplets, each triplet is a triangle</param>
void ConvertFaceToFaceIndex(
	const std::vector<Float3>& inRawVertices,
	std::vector<Float3>& outVertices,
	std::vector<unsigned int>& outFaces);

/// <summary>
/// Reads a face file (polygon soup) into a data structure.
/// O(n) where n is the number of vertices.
/// </summary>
/// <param name="inFilePath">path to an existing file in face format (polygon soup)</param>
/// <param name="outRawVertices">vector of non-unique vertex positions, each triplet is a triangle (polygon soup)</param>
/// <returns>true if file can be read and is formatted correctly, false otherwise</returns>
bool ReadFaceFile(
	const std::string& inFilePath,
	std::vector<Float3>& outRawVertices);

/// <summary>
/// Writes data structure into a face-index file (.face).
/// O(n) where n is the number of vertices.
/// </summary>
/// <param name="inVertices">vector of unique vertex positions</</param>
/// <param name="inFaces">vector of vertex index triplets, each triplet is a triangle</param>
/// <param name="outFilePath">destination to write a file in face-index format (.face)</param>
/// <returns>true if file is written, false otherwise</returns>
bool WriteFaceIndexFile(
	const std::vector<Float3>& inVertices,
	const std::vector<unsigned int>& inFaces,
	const std::string& outFilePath);

/// <summary>
/// Converts a face format file in into a face-index format file.
/// </summary>
/// <param name="faceFilePath">path to an existing file in face format (polygon soup)</param>
/// <param name="faceIndexFilePath">destination to write a file in face-index format (.face)</param>
/// <returns>true if a face-index file was written, false otherwise</returns>
bool ConvertFaceToFaceIndexFile(
	const std::string& faceFilePath,
	const std::string& faceIndexFilePath);

/// <summary>
/// Compares a face data structure to a face-index data structure.
/// </summary>
/// <param name="rawVertices">vector of non-unique vertex positions, each triplet is a triangle (polygon soup)</param>
/// <param name="vertices">vector of unique vertex positions</param>
/// <param name="faces">vector of vertex index triplets, each triplet is a triangle</param>
/// <returns>true if the both structures are equivalent, false otherwise</returns>
bool CompareFaceAndFaceIndex(
	const std::vector<Float3>& rawVertices,
	const std::vector<Float3>& vertices,
	const std::vector<unsigned int>& faces);