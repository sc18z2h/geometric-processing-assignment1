To compile:
$ make

To run:
$ ./face2faceindex [-d <output directory>] file...
$ ./faceindex2directededge [-d <output directory>] file...

Both tools can accept one or more files:
$ ./face2faceindex file1.tri file2.tri file3.tri

Each file converted will change its extension appropriately. E.g. :
face2faceindex changes its extensions to *.face
faceindex2directededge changes its extensions to *.diredge
And the files are placed in the same directory.

To change the output directory use the -d flag:
$ ./face2faceindex -d faces/ file1.tri file2.tri file3.tri


Algorithmic complexity:
face2faceindex 		O(n log(n)) where n is the number of vertices.
faceindex2directededge 	O(n log(n)) where n is the number of vertices.
faceindex2directededge (without connectivity check)	O(n) where n is the number of vertices.
