/*
///////////////////////////////////////////////////

	Zecheng Hu
	October, 2021

	------------------------
	FaceIndexToDirectedEdge.h
	------------------------
	
	Converts face-index format files (.face) into directed-edge files (.diredge).

///////////////////////////////////////////////////
*/

#pragma once
#include <vector>
#include <string>
#include "Float3.h"

/// <summary>
/// Converts face-index format to directed-edge format.
/// O(n) where n = number of vertices.
/// </summary>
/// <param name="inVertices">vector of unique vertex positions</param>
/// <param name="inFaces">vector of vertex index triplets, each triplet is a triangle</param>
/// <param name="outFirstDirectedEdge">outFirstDirectedEdge[vertexID] gives a directed edge from that vertex</param>
/// <param name="outPairedDirectedEdge">outPairedDirectedEdge[edgeID] gives its paired directed edge going in the opposite direction</param>
/// <returns>true if all edges share 2 faces, false otherwise</returns>
bool ConvertFaceIndexToDirectedEdge(
	const std::vector<Float3>& inVertices,
	const std::vector<unsigned int>& inFaces,
	std::vector<unsigned int>& outFirstDirectedEdge,
	std::vector<unsigned int>& outPairedDirectedEdge);

/// <summary>
/// Reads from a face-index file (.face) into data structures.
/// O(n) where n = number of lines.
/// </summary>
/// <param name="inFilePath">path to an existing file in face-index format (.face)</param>
/// <param name="outVertices">vector of unique vertex positions</param>
/// <param name="outFaces">vector of vertex index triplets, each triplet is a triangle</param>
/// <returns>true if file can be read and is formatted correctly, false otherwise</returns>
bool ReadFaceIndexFile(
	const std::string& inFilePath,
	std::vector<Float3>& outVertices,
	std::vector<unsigned int>& outFaces);

/// <summary>
/// Writes data structures into a directed-edge file (.diredge).
/// O(n) where n = number of vertices.
/// </summary>
/// <param name="inVertices">vector of unique vertex positions</param>
/// <param name="inFaces">vector of vertex index triplets, each triplet is a triangle</param>
/// <param name="inFirstDirectedEdge">inFirstDirectedEdge[vertexID] gives a directed edge from that vertex</param>
/// <param name="inPairedDirectedEdge">inPairedDirectedEdge[edgeID] gives its paired directed edge going in the opposite direction</param>
/// <param name="outFilePath">destination to write a file in directed-edge format (.diredge)</param>
/// <returns>true if file is written, false otherwise</returns>
bool WriteDirectedEdgeFile(
	const std::vector<Float3>& inVertices,
	const std::vector<unsigned int>& inFaces,
	const std::vector<unsigned int>& inFirstDirectedEdge,
	const std::vector<unsigned int>& inPairedDirectedEdge,
	const std::string& outFilePath);

/// <summary>
/// Converts a face-index format file in into a directed-edge format file.
///	O(n) where n = number of vertices.
/// </summary>
/// <param name="faceIndexFilePath">path to an existing file in face-index format (.face)</param>
/// <param name="directedEdgeFilePath">destination to write a file in directed-edge format (.diredge)</param>
/// <returns>true if a directed-edge file was written, false otherwise</returns>
bool ConvertFaceIndexToDirectedEdgeFile(
	const std::string& faceIndexFilePath,
	const std::string& directedEdgeFilePath);

/// <summary>
/// Checks for pinch vertices inside the data structure. Only works when allEdgesShare2Faces. Otherwise it can stall.
/// O(n) where n = number of vertices.
/// </summary>
/// <param name="vertices">vector of unique vertex positions</param>
/// <param name="faces">vector of vertex index triplets, each triplet is a triangle</param>
/// <param name="pairedDirectedEdge">pairedDirectedEdge[edgeID] gives its paired directed edge going in the opposite direction</param>
/// <returns>true if NO pinch edges found, false otherwise</returns>
bool CheckPinchVertices(
	const std::vector<Float3>& vertices,
	const std::vector<unsigned int>& faces,
	const std::vector<unsigned int>& pairedDirectedEdge);

/// <summary>
/// Compares a face-index data structure to a directed-edge data structure.
/// O(n) where n = number of vertices.
/// </summary>
/// <param name="vertices">vector of unique vertex positions</param>
/// <param name="faces">vector of vertex index triplets, each triplet is a triangle</param>
/// <param name="firstDirectedEdge">firstDirectedEdge[vertexID] gives a directed edge from that vertex</param>
/// <param name="pairedDirectedEdge">pairedDirectedEdge[edgeID] gives its paired directed edge going in the opposite direction</param>
/// <returns>true if the both structures are equivalent, false otherwise</returns>
bool CompareFaceIndexAndDirectedEdge(
	const std::vector<Float3>& vertices,
	const std::vector<unsigned int>& faces,
	const std::vector<unsigned int>& firstDirectedEdge,
	const std::vector<unsigned int>& pairedDirectedEdge);

/// <summary>
/// Evaluates whether the given mesh is connected or not.
/// O(n log(n)) where n = number of vertices.
/// </summary>
/// <param name="vertices">vector of unique vertex positions</param>
/// <param name="faces">vector of vertex index triplets, each triplet is a triangle</param>
/// <param name="pairedDirectedEdge">pairedDirectedEdge[edgeID] gives its paired directed edge going in the opposite direction</param>
/// <returns>true if connected, false otherwise</returns>
bool IsConnected(
	const std::vector<Float3>& vertices,
	const std::vector<unsigned int>& faces,
	const std::vector<unsigned int>& pairedDirectedEdge);

/// <summary>
/// Calculates the genus of the mesh using the Euler formula. Only works when the mesh is connected.
/// O(1).
/// </summary>
/// <param name="vertices">vector of unique vertex positions</param>
/// <param name="faces">vector of vertex index triplets, each triplet is a triangle</param>
/// <returns>the genus</returns>
int GetGenus(
	const std::vector<Float3>& vertices,
	const std::vector<unsigned int>& faces);