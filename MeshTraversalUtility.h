/*
///////////////////////////////////////////////////

	Zecheng Hu
	October, 2021

	------------------------
	MeshTraversalUtility.h
	------------------------

	Utility functions that help with traversing directed-edge data structures.

///////////////////////////////////////////////////
*/

#pragma once
#include <vector>

/// <summary>
/// Traverse a directed edge around its triangle, counter-clockwise.
/// O(1).
/// </summary>
/// <param name="edgeID">index of the directed edge</param>
/// <param name="steps">number of steps to take, can be negative</param>
/// <returns>edgeID after taking steps</returns>
inline unsigned int TraverseDirectedEdgeAroundTriangle(
	unsigned int edgeID, 
	int steps = 1)
{
	unsigned int face = edgeID / 3;
	// we want the part that is undivisible by 3
	int remainder = (static_cast<int>(edgeID) + steps) % 3;
	// modulo can produce negatives
	// loop back into [0-2] if remainder falls out of range
	if (remainder < 0)
		remainder += 3;
	return face * 3 + static_cast<unsigned int>(remainder);
}

/// <summary>
/// Traverse a directed edge around one of its connected vertex.
/// O(n) where n is the number of steps.
/// </summary>
/// <param name="edgeID">index of the directed edge to traverse</param>
/// <param name="aroundVertexID">traverse around this vertex</param>
/// <param name="faces">vector of vertex index triplets, each triplet is a triangle</param>
/// <param name="pairedDirectedEdge">pairedDirectedEdge[edgeID] gives its paired directed edge going in the opposite direction</param>
/// <param name="steps">number of steps to take, positive=counter-clockwise around vertex, negative=clockwise around vertex</param>
/// <returns>edgeID after taking steps, or -1 if it cannot be traversed</returns>
inline unsigned int TraverseDirectedEdgeAroundVertex(
	unsigned int edgeID,
	unsigned int aroundVertexID,
	const std::vector<unsigned int>& faces,
	const std::vector<unsigned int>& pairedDirectedEdge,
	int steps = 1)
{
	if (steps == 0)
		return edgeID;
	// remember if steps was negative
	bool isReversed = steps < 0;
	// make steps absolute
	if(isReversed)		
		steps = -steps;

	for (int i = 0; i < steps && edgeID != static_cast<unsigned int>(-1); ++i)
	{
		unsigned int fromVertexID = faces[edgeID];

		if (isReversed)
		{
			// traverse clockwise around vertex
			if (fromVertexID == aroundVertexID)
				edgeID = pairedDirectedEdge[edgeID];
			else
				edgeID = TraverseDirectedEdgeAroundTriangle(edgeID, -1);
		}
		else
		{
			// traverse counter-clockwise around vertex
			if (fromVertexID == aroundVertexID)
				edgeID = TraverseDirectedEdgeAroundTriangle(edgeID, -1);
			else
				edgeID = pairedDirectedEdge[edgeID];
		}
	}

	return edgeID;
}