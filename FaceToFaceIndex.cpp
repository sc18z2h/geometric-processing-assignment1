/*
///////////////////////////////////////////////////

	Zecheng Hu
	October, 2021

	------------------------
	FaceToFaceIndex.cpp
	------------------------

	Converts face format files (polygon soup) into face-index files (.face).

///////////////////////////////////////////////////
*/

#include "FaceToFaceIndex.h"
#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <map>
#include <cstring>
using namespace std;

int main(int argc, const char** argv)
{
	if (argc < 2)
	{
		cout << "Usage: face2faceindex [-d <output directory>] file..." << endl;
		return EXIT_FAILURE;
	}

	int i = 1;
	bool hasOutputDirectory = false;
	if (strcmp(argv[1], "-d") == 0)
	{
		hasOutputDirectory = true;
		i = 3;
	}

	int exitCode = EXIT_SUCCESS;
	bool moreThan1File = i + 1 < argc;

	for (; i < argc; ++i)
	{
		string inFilePath(argv[i]);
		stringstream outFilePath;

		size_t lastDotIndex = inFilePath.find_last_of(".");

		if (hasOutputDirectory)
		{
			size_t lastForwardSlash = inFilePath.find_last_of("/");
			size_t lastBackSlash = inFilePath.find_last_of("\\");
			size_t lastSlashIndex = max(lastForwardSlash + 1, lastBackSlash + 1);
			outFilePath << argv[2] <<
				// file path without directory without extension
				inFilePath.substr(lastSlashIndex, lastDotIndex - lastSlashIndex) <<
				".face";
		}
		else
		{
			outFilePath <<
				// file path without extension
				inFilePath.substr(0, lastDotIndex) <<
				".face";
		}

		const char* lineSeperator = "====================================";
		if (moreThan1File)
			cout << "Converting (" << inFilePath << ")." << endl;

		if (ConvertFaceToFaceIndexFile(inFilePath, outFilePath.str()))
			cout << "Conversion done." << endl;
		else
			exitCode = EXIT_FAILURE;

		if (moreThan1File)
			cout << lineSeperator << endl;
	}

	return exitCode;
}

void ConvertFaceToFaceIndex(
	const std::vector<Float3>& inRawVertices,
	std::vector<Float3>& outVertices,
	std::vector<unsigned int>& outFaces)
{
	outVertices.clear();
	outFaces.clear();

	outFaces.reserve(inRawVertices.size());
	// F = inRawVertices.size() / 3
	// Euler's formula estimate V ~ F/2
	outVertices.reserve(inRawVertices.size() / (3 * 2));

	// map from vertex positions (float3) to vertexID, each entry is unique
	map<Float3, unsigned int> vertexPositionToVertexID;

	// search for unique vertex positions
	for (size_t i = 0; i < inRawVertices.size(); ++i)
	{
		map<Float3, unsigned int>::iterator iterator = vertexPositionToVertexID.find(inRawVertices[i]);
		if (iterator == vertexPositionToVertexID.end())
		{
			// vertex position is unique, add to map
			outVertices.push_back(inRawVertices[i]);
			outFaces.push_back(outVertices.size() - 1);
			vertexPositionToVertexID[inRawVertices[i]] = outVertices.size() - 1;
		}
		else
		{
			// vertex position already exists, reuse its vertexID
			outFaces.push_back((*iterator).second);
		}
	}
}

bool ReadFaceFile(
	const std::string& inFilePath,
	std::vector<Float3>& outRawVertices)
{
	outRawVertices.clear();

	string line;
	ifstream fileStream(inFilePath);

	if (fileStream.is_open() == false)
	{
		cerr << "[Error] Couldn't open file '" << inFilePath << "'." << endl;
		return false;
	}

	size_t numberOfFaces = 0;
	if (fileStream >> numberOfFaces)
		outRawVertices.reserve(numberOfFaces * 3);

	while (fileStream.good())
	{
		Float3 newVertex;
		if (fileStream >> newVertex.x >> newVertex.y >> newVertex.z)
			outRawVertices.push_back(newVertex);
	}
	
	// if we've reached the end of the file,
	// any formatting errors will stop reading before the end
	if (fileStream.eof())
		return true;
	else
	{
		cerr << "[Error] File has bad format '" << inFilePath << "'." << endl;
		return false;
	}
}

bool WriteFaceIndexFile(
	const std::vector<Float3>& inVertices,
	const std::vector<unsigned int>& inFaces,
	const std::string& outFilePath)
{
	ofstream fileStream(outFilePath);

	if (fileStream.good())
	{
		// use the file name without directory and without extension as the object name
		size_t lastForwardSlash = outFilePath.find_last_of("/");
		size_t lastBackSlash = outFilePath.find_last_of("\\");

		size_t lastSlashIndex = max(lastForwardSlash + 1, lastBackSlash + 1);
		size_t lastDotIndex = outFilePath.find_last_of(".");
		string objectName = outFilePath.substr(lastSlashIndex, lastDotIndex - lastSlashIndex);

		fileStream <<
			"# University of Leeds 2021-2022" << endl <<
			"# COMP 5812M Assignment 1" << endl <<
			"# Zecheng Hu" << endl <<
			"# 201249146" << endl <<
			"#" << endl <<
			"# Object Name: " << objectName << endl <<
			"# Vertices=" << inVertices.size() <<
			" Faces=" << inFaces.size() / 3 << endl <<
			"#" << endl;

		// float write format settings
		fileStream << fixed << setprecision(6);
		const char* SPACE = "  ";

		// make every vertex index the same width as the last index
		string lastVertexIndex = to_string(inVertices.size() - 1);
		size_t vertexIndexWidth = lastVertexIndex.size();
		for (size_t i = 0; i < inVertices.size(); ++i)
		{
			fileStream <<
				"Vertex " << setw(vertexIndexWidth) << i << SPACE <<
				inVertices[i].x << SPACE <<
				inVertices[i].y << SPACE <<
				inVertices[i].z << endl;
		}

		// make every face index the same width as the last index
		string lastFaceIndex = to_string(inFaces.size() / 3 - 1);
		size_t faceIndexWidth = lastFaceIndex.size();
		for (size_t i = 0; i + 2 < inFaces.size(); i += 3)
		{
			fileStream <<
				"Face " << setw(faceIndexWidth) << i / 3 << SPACE <<
				setw(vertexIndexWidth) << inFaces[i] << SPACE <<
				setw(vertexIndexWidth) << inFaces[i + 1] << SPACE <<
				setw(vertexIndexWidth) << inFaces[i + 2] << endl;
		}

		// file stream is only good when all writing has succeeded
		if (fileStream.good())
			return true;
	}
	cerr << "[Error] Couldn't write file '" << outFilePath << "'." << endl;
	return false;
}

bool ConvertFaceToFaceIndexFile(
	const std::string& faceFilePath,
	const std::string& faceIndexFilePath)
{
	vector<Float3> rawVertices;
	bool isWritten = false;
	if (ReadFaceFile(faceFilePath, rawVertices))
	{
		vector<Float3> vertices;
		vector<unsigned int> faces;
		ConvertFaceToFaceIndex(rawVertices, vertices, faces);

		// this is a sanity check to make sure data structure is organised as expected
		// if this test fails, its probably a bug in the code
		if (CompareFaceAndFaceIndex(rawVertices, vertices, faces) == false)
			cerr << 
				"[ERROR!] Sanity check failed! (" << faceFilePath << 
				"). Face format isn't equivalent to face-index format. Probably a bug in the code." << endl;

		isWritten = WriteFaceIndexFile(vertices, faces, faceIndexFilePath);
	}
	return isWritten;
}

bool CompareFaceAndFaceIndex(
	const std::vector<Float3>& rawVertices, 
	const std::vector<Float3>& vertices, 
	const std::vector<unsigned int>& faces)
{
	if (rawVertices.size() != faces.size())
		return false;

	// make sure vertexIDs match to their corresponding value
	for (size_t i = 0; i < rawVertices.size(); ++i)
		if (rawVertices[i] != vertices[faces[i]])
			return false;

	return true;
}
